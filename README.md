Dieses Skript erleichtert das Eintragen der Punkte in das aprove-System.

# Installation
Es kann in den Browser eingebunden werden, indem ein neues Lesezeichen mit dem Code des Skriptes statt einer URL angelegt wird. Über dieses Lesezeichen kann das Skript dann gestartet werden.
Bei der neuen Version ist es zudem wichtig, die Korrekte Variante auszuwählen. Dafür muss man lediglich für die gewünschte Variante in Loop oder Once den Kommentarblock entfernen und die andere Variante auskommentieren
Variante 1 ist Standardmäßig ausgewählt.
# Verwendung
1. Öffne die Punkte-Management-Seite
2. Nutze die Filter-Funktion, um die die Ansicht auf die gewünschte Hausaufgabe/Prüfung zu beschrenken
3. Starte das Skript durch Aufrufen des Lesezeichens
4. Wenn Variante 2 gewählt wird, muss zunächsts der Gruppenstring eingelesen werden.
5. Gebe alle Matrikelnummern oder die Gruppennummer einer Gruppe durch Kommata getrennt an, und dahinter durch Lücken getrennt die erreichten Punktzahlen.

# Syntax
Die Eingabe für Variante 2 sollte also zunächst die Form [Matrikelnummer 1],[Matrikelnummer 2] [Matrikelnummer 3],[Matrikelnummer 4] ..." haben. "[Matrikelnummer 1],[Matrikelnummer 2]" sind dabei Gruppe 1 usw.
Weitere Eingaben haben dann die Form "[Guppennummer] Punktzahl1 Punktzahl2 ...", z.B. "1 4 7 11".
Bei Variante 1 können Eingaben zusätzlich auch die Form "[Matrikelnummer 1],[Matrikelnummer 2],... [Punkte Aufgabe 1] [Punkte Aufgabe 2] ..." haben.  
Also z.B. "111111,222222,333333 4 5,5 6 7", wenn die drei Studenten mit den Matrikelnummern 111111, 222222 und 333333 zusammen abgegeben haben, und in den 4 Aufgaben 4, 5.5, 6, bzw. 7 Punkte erreicht haben. Als Dezimaltrennzeichen kann für die Punkte sowohl ein Komma, als auch ein Punkt verwendet werden - die Ausgabe erfolgt in jedem Fall mit Punkten, da dies von aprove so verlangt wird.

# Fehlerbehandlung
Ist die Eingabe syntaktisch nicht korrekt, wird sie verworfen.  
Wird ein Student nicht gefunden, wird das entsprechend angezeigt. Wurden mehrere Matrikelnummern angegeben, werden die Punkte bei allen anderen gefundenen Studenten eingetragen.

# Welche Variante des Skriptes ist die richtige?
Die beiden Varianten unterscheiden sich nur darin, bei Variante 1 die Gruppen innerhalb des Quellcodes hardcoded werden müssen, während Variante 2 den Prozess als dynamischen String annimmt. Diesen kann man sich z.B. irgendwo abspeichern und so sehr schnell übergeben und leicht bearbeiten. 
Zudem gibt es eine Unterscheidung in "Loop" und "Once", welche sich darin unterscheiden, dass "Loop" nach dem Eintragen der Punkte für die erste Gruppe direkt die Punkte für die nächste Gruppe erfragt, "Once" muss hingegen für jede Gruppe neu aufgerufen werden. Das kann sinnvoll sein, wenn nicht alle Punkte für alle Studenten gleichzeitig eingegeben werden sollen.

# Sicherheit
Das Skript sendet keinerlei Datei an externe Server, die Verarbeitung läuft komplett im Browser. Die Punkte der Studenten werden also nicht weitergegeben.  
Auch wenn ich das Skript eingehed getestet habe, kann ich jedoch nicht für die Korrektheit garantieren, die Verwendung erfolgt auf eigene Gefahr.

# Fehler gefunden?
Fehler bitte hier über das Issue-System, oder per Mail an Christopher.Brix@rwth-aachen.de melden.