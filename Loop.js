javascript:(function() {
    /* Load the script */
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    /* Poll for jQuery to come into existance */
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 100);
        }
    };

    /* Start polling... */
    checkReady(function($) {
        $.expr[':'].textEquals = $.expr.createPseudo(function(arg) {
            return function( elem ) {
                return $(elem).text().match("^" + arg + "$");
            };
        });
        /*
        //Das geht schnell, wenn man sich die Gruppen in einem Dokument abspeichert,
        //ist einfacher zu veraendern als der hardcoded switch-case
        var groups = promt("Gib die Gruppen bitte in der Form 'Mat1,Mat2,...,MatN Mat1,Mat2,...,MatN' ein. Die Nummerierung erfolg durch die Eingabe","");
        groups = groups.split(' ');
        */
        while(1) {
            var data = prompt("Matrikelnummern (durch Kommata getrennt) und Punkte (durch Lücken getrennt) eingeben ('exit' um zu beenden)", "");
            if(data ==='exit') {
                return;
            }

            var input = data.split(' ');
            var ids;
            //Variante 1: Alle Gruppen werden in den Switch-Case geschrieben. Wenn die Gruppen sich aendern,
            //muss dies hier reflektiert werden. Aktuelle Werte sind Beispiele und sollten ersetzt werden.
            switch(input[0]){
              case "1": ids = ["123456", "654321"];
                  break;
              case "2": ids = ["373737"];
                  break;
              case "3": ids = ["111111", "222222", "333333"];
                  break;
              /* Additional Grops here*/
              default: ids = input[0].split(',');
                  break;
            }
            
            //Variante 2: Die Gruppen werden via copy-paste im ersten schritt uebergeben
            //und koennen dann in diesem Schritt abgerufen werden
            /*
            ids = groups[parseInt(input[0])];
            ids = ids.split(',');
            */

            if($('form td input[type="text"]').last().closest('tr').find('input[type="text"]').length !== input.length - 1) {
                console.log($('form td input[type="text"]').last().closest('tr').find('input[type="text"]').length);
                alert('Ungültige Eingabe - Bitte die Anzeige auf die entsprechende Übung beschränken, und für jede Aufgabe Punkte angeben!');
                continue;
            }

            for (var i = 0; i < ids.length; i++) {
                var fields = $('form td:textEquals("' + ids[i] + '")').parent().find('input[type="text"]');
                var numberOfFields = $(fields).length;
                if(numberOfFields === 0) {
                    alert('Matrikelnummer ' + ids[i] + ' wurde nicht gefunden!');
                    continue;
                }
                
                for (var j = 0; j < numberOfFields; j++) {
                    var points = input[j+1].replace(',', '.');
                    $(fields).eq(j).val(points);
                }
            }
        }
    });
})();